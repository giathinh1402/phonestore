import React, { Component } from 'react';
import {BrowserRouter ,Route,Link , Switch} from 'react-router-dom' ;// npm i react-router-dom
const Home = React.lazy(()=> import('./homePage/Home'));
const Login = React.lazy(()=> import('./homePage/Login'));
const Register = React.lazy(()=> import('./register/Register'));
const NotFound = React.lazy(()=> import('./notfound/NotFound'));

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
export default class Router extends Component {
    render() {
        return (
            <BrowserRouter>
                <React.Suspense fallback={loading}>
                    <Switch>
                        <Route exact path="/#" name="Home" render={props => <Home {...props} />} />
                        <Route exact path="/Login" name="Login" render={props => <Login {...props} />} />
                        <Route exact path="/Register" name="Register" render={props => <Register {...props} />} />
                        <Route path="" name="NotFound" render={props => <NotFound {...props} />} />
                    </Switch>
                </React.Suspense>
            </BrowserRouter>
        )
    }
}
