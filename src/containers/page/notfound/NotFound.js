import React, { Component } from 'react'

class NotFound extends Component {
    render() {
        return (
            <div className="container justify-content-center">
                <center><h1> 404 - Not Found</h1></center>
            </div>
        )
    }
}

export default NotFound;
