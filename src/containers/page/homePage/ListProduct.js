import React, { Component } from 'react'
import ProductsContainer from'./../../components/products/ProductsContainer';
import MessageContainer from '../../components/message/MessageContainer';

class ListProduct extends Component {
    render() {
        const keyWord = this.props.match.params.search;
        return (
            <div className="container">
                <MessageContainer />
                <ProductsContainer keyWord={keyWord}
                />      
            </div>
        )
    }
}

export default ListProduct;
