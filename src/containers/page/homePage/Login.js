import React, { Component } from 'react';


class Login extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            email: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        this.setState({ 
            [e.target.id]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        var {email, password} =this.state;
        if(email === 'admin' && password === 'admin'){
            localStorage.setItem('email',JSON.stringify({
                email : email,
                password : password
            }));
        }
            
    }
 
    render() {
        
        return (
          
            <div className="container">
                <form onSubmit={this.handleSubmit} action="" className="white"></form>
                <h5 className="grey-text text-darken-3"> Login</h5>
                <div className="input-field">
                    <label htmlFor="email">Email</label>
                    <input type="email" id ="email" onChange ={this.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="password">PassWord </label>
                    <input type="password" id ="password" onChange ={this.handleChange}/>
                </div>
                <div className ="input-field">
                    <button className="btn purple lighten-1 z-depth-0"onClick={this.handleSubmit}>LOGIN</button>
                </div>
            </div>
        )
    }
}
export default Login;
