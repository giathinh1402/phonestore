import React,{Component} from 'react';
import './slide.css';

class Slide extends Component {

    constructor(props) {
        super(props);

        this.state = {
    
        };
        
    }   

    render(){
        return (
            <div id="demo" className="carousel slide" data-ride="carousel">
                <ul className="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" className="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <img src="https://cdn.tgdd.vn/Files/2019/04/06/1159141/realme-3_800x450.jpg" 
                    alt="Realme 3" width="1400" height="700" />
                    <div className="carousel-caption">
                        <h1>Realme 3</h1>
                        <h3>We give you more fashion phone you need!</h3>
                    </div>   
                </div>
                <div className="carousel-item">
                    <img src="https://photo2.tinhte.vn/data/attachment-files/2019/06/4697848_cover_home_lua_chon_thoai_theo_tam_gia.jpg" 
                    alt="K20 Pro" width="1400" height="700" />
                    <div className="carousel-caption">
                        <h1>K20 Pro</h1>
                        <h3>New design phone hot 2019!</h3>
                    </div>   
                </div>
                <div className="carousel-item">
                    <img src="https://cdn.tgdd.vn/Files/2016/03/03/796410/firkraag_mobile_concept_3.jpg" 
                    alt="Iphone Xs Max" width="1400" height="700" />
                    <div className="carousel-caption">
                        <h1>Iphone Xs Max</h1>
                        <h3>We have more than you need!</h3>
                    </div>   
                </div>
            </div>
            <a className="carousel-control-prev" href="#demo" data-slide="prev">
                <span className="carousel-control-prev-icon"></span>
            </a>
            <a className="carousel-control-next" href="#demo" data-slide="next">
                <span className="carousel-control-next-icon"></span>
            </a>
</div>
            );
        }   
    }

    export default Slide;
