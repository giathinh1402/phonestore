import React,{Component} from 'react';


class Footer extends Component {
    render(){
        return ( 
            <footer className="page-footer center-on-small-only">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-6 ml-5">
                            <h5 className="title social-section-title">Social Media</h5>
                            <div className="social-section text-md-left">
                                <ul className="text-center">
                                    <li>
                                        <a href="/#" className="btn-floating  btn-fb waves-effect waves-light">
                                            <i className="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-ins waves-effect waves-light">
                                            <i className="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-tw waves-effect waves-light">
                                            <i className="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-yt waves-effect waves-light">
                                            <i className="fa fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-li waves-effect waves-light">
                                            <i className="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-dribbble waves-effect waves-light">
                                            <i className="fa fa-dribbble left"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-pin waves-effect waves-light">
                                            <i className="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/#" className="btn-floating  btn-gplus waves-effect waves-light">
                                            <i className="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-2">
                            <h5 className="title">Delivery</h5>
                            <ul>
                                <li>
                                    <a href="/#">Store Delivery</a>
                                </li>
                                <li>
                                    <a href="/#">Online Delivery</a>
                                </li>
                                <li>
                                    <a href="/#">Delivery Terms &amp; Conditions</a>
                                </li>
                                <li>
                                    <a href="/#">Tracking</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-lg-2">
                            <h5 className="title">Need help?</h5>
                            <ul>
                                <li>
                                    <a href="/#">FAQ</a>
                                </li>
                                <li>
                                    <a href="/#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="/#">Return Policy</a>
                                </li>
                                <li>
                                    <a href="/#">Product Registration</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
