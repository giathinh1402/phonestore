import React,{Component} from 'react';
import Headers from './containers/components/Header/Header';
import Footer from './containers/components/footer/Footer';

import './App.css';

class App extends Component {
    render(){      
        return (
            <div>     
                <main id="mainContainer">
                    <div className="container"> 
                        <Headers />
                        <Footer />
                    </div>
                    
                </main>
                
            </div>
        );
    }
}

export default App;
